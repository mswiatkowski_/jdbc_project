package entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="users")
@Getter
@Setter
@ToString
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // baza zajmuje sie tworzeniem id (autoincrement)
    private Integer id;

    private String login;
    private  String name;
    private String surname;
    private String password;
    private String email;
    private LocalDateTime registrationDate;

    @Column(name = "active")
    private boolean isActive;

}