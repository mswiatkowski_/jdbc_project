package entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Setter
@Getter
@Table(name = "locations")
@ToString
public class Location {

    @Id
    @Column(name = "id")
    private int id;

    private String city;
    private String address;
    private String name;

}
