package entities;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Scanner;

public class MainUsers {

    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory =
                Persistence.createEntityManagerFactory("HibernateDBConnection");

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        User user = createUser();
        saveToDatabase(entityManager, user);
        showUsers(entityManager);

    }

    private static void saveToDatabase(EntityManager entityManager, User user) {
        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
    }

    private static User createUser() {

        Scanner scanner = new Scanner(System.in);
        User user = new User();

        System.out.print("Type login: ");
        user.setLogin(scanner.nextLine());

        System.out.print("Type name: ");
        user.setName(scanner.nextLine());

        System.out.print("Type surname: ");
        user.setSurname(scanner.nextLine());

        System.out.print("Type password: ");
        user.setPassword(scanner.nextLine());

        System.out.print("Type email: ");
        user.setEmail(scanner.nextLine());

        user.setRegistrationDate(LocalDateTime.now());
        return user;

    }

    private static void showUsers(EntityManager entityManager) {

        List<User> usersList = entityManager.createQuery("FROM User", User.class).getResultList();
        usersList.forEach(u -> {
            System.out.println(String.format("%s - %s - %s - %s", u.getLogin(), u.getName(), u.getSurname(), u.getEmail()));
        });

    }

}
