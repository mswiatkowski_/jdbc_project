import java.sql.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        try {

            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc?serverTimezone=Europe/Warsaw", "jdbcuser", "dD3heD6kfD8wg");
//            addDataAbaoutLocation2(conn);
//            System.out.println();
//            showLocationList(conn);

            insertEvent(conn);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void showLocationList(Connection connection) throws Exception {

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM locations");

        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String city = resultSet.getString("city");
            String address = resultSet.getString("address");
            String name = resultSet.getString("name");

            System.out.println(id + " " + name + " " + city + " " + address);
        }

        resultSet.close();
        statement.close();

    }

    public static void addDataAbaoutLocation(Connection connection) throws SQLException{

        Statement statement = connection.createStatement();
        Scanner scanner = new Scanner(System.in);

        System.out.print("Type city: ");
        String city = scanner.nextLine();

        System.out.print("Type address: ");
        String address = scanner.nextLine();

        System.out.print("Type name: ");
        String name = scanner.nextLine();

        int result = statement.executeUpdate(String.format("INSERT INTO locations VALUES (NULL, '%s', '%s', '%s')", city, address, name));

        statement.close();

    }

    public static void addDataAbaoutLocation2(Connection connection) throws SQLException{

        PreparedStatement statement = connection.prepareStatement("INSERT INTO locations VALUES (NULL, ?, ?, ?)");
        Scanner scanner = new Scanner(System.in);

        System.out.print("Type city: ");
        String city = scanner.nextLine();

        System.out.print("Type address: ");
        String address = scanner.nextLine();

        System.out.print("Type name: ");
        String name = scanner.nextLine();

        statement.setString(1, city);
        statement.setString(2, address);
        statement.setString(3, name);

        statement.executeUpdate();
        statement.close();

    }

    private static void insertEvent(Connection connection) throws Exception {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Type event name: ");
        String eventName = scanner.nextLine();
        System.out.print("Type start date: ");
        String startDate = scanner.nextLine();
        System.out.print("Type end date: ");
        String endDate = scanner.nextLine();

        System.out.println();

        showLocationList(connection);

        System.out.println();
        System.out.print("Pick one location: ");
        int locationId = scanner.nextInt();

        String sqlQuery = "INSERT INTO events VALUES (NULL, ?, ?, ?, ?)";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);

        preparedStatement.setString(1, eventName);
        preparedStatement.setString(2, startDate);
        preparedStatement.setString(3, endDate);
        preparedStatement.setInt(4, locationId);

        preparedStatement.executeUpdate();
        preparedStatement.close();

    }

}
